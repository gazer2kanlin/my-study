Java 实作 Bézier Curve 
===

## 理论实现

### 推导

1. 两点构成一线段, Linear Bézier Curves, 一阶 (lerp)

    a. $\vec{V_{ab}}=P_b-P_a$

    b. $P_t=P_a + t\vec{V_{ab}}=(1-t)P_a + tP_b$

2. 三点构成二线段, Quadratic Bézier Curves, 二阶

    a1. $\vec{V_{12}}=P_2-P_1$

    a2. $\vec{V_{23}}=P_3-P_2$

    a3. $P_a=P_1 + t\vec{V_{12}}$

    a4. $P_b=P_2 + t\vec{V_{23}}$
    
    至此点数从 __三点变成两点__，利用步骤一推导得到

    b1. $\vec{V_{ab}}=P_b-P_a$

    b2. $P_t=P_a + t\vec{V_{ab}}$

3. 根据上述推导，可以利用递归的方式，求得 N 阶解。

### 实作

以 Java 实作上述流程：

```java
public Pt find(List<Pt> pts, double t) {
    if(pts.size() == 1) {
        return pts.get(0);
    }

    List<Pt> result = new ArrayList<>();
    for(int i = 1; i < pts.size(); i++) {
        result.add(lerp(pts.get(i - 1), pts.get(i), t));
    }
    return find(result);
}

public Pt lerp(Pt start, Pt end, double t) {
    Vec v = start.vecTo(end);
    return start.move(v.multi(t));
}
```

## 进阶理论

除了上述方式，亦可用公式描述 Bézier Curve

$B_n(t) = \sum_{i=0}^n \binom{n}{i} (1-t)^{(n-i)} t^i P_i$

其中 n 为 __总点数减一__。

例如：

* 三点二线 $B_2(t) = (1-t)^2 P_0 + 2(1-t) t P_1 + t^2 P_2$  
* 四点三线 $B_3(t) = (1-t)^3 P_0 + 3(1-t)^2 t P_1 + 3(1-t) t^2 P_2 + t^3 P_3$  

当对四点三线进行微分时：

* $B_3'(t) = \displaystyle \frac{\partial B_3}{\partial t} = (1-t)^2Q_0 + 2(1-t)tQ_1 + t^2Q_2 = Q_2(t)$   其中 $Q_x$ 为：
  * $Q_0 = P_1 - P_0$
  * $Q_1 = P_2 - P_1$
  * $Q_2 = P_3 - P_2$

明显的，$B_n(t)$ 的微分方程式结构等于 __减一阶__ 的 $B_{(n-1)}(t)$ 方程式，求得的值等于对应位置的切向向量。

例如 t=-0.2

* $Pt = B_4(0.2)$
* $\vec{V} = Q_3(0.2)$

$\vec{V}$ 为 Bézier Curve 上 $Pt$ 点的切线向量。真是很神奇的呢。

有了 $\vec{V}$，就能得到法线向量，进而求得距离 $Pt$ 某个距离的其他点座标。




## 参考

* [Bézier Curve Wikipedia](https://en.wikipedia.org/wiki/B%C3%A9zier_curve)
* [The Beauty of Bézier Curves](https://www.youtube.com/watch?v=aVwxzDHniEw&t=25s)