光罩计划 Mask Plan
===

## 需求 Requirement
根据当站与未来的批次，规划「曝光机群」的整体光罩挂载计划，以及可能的作业顺序。

* 限制条件
  * 曝光机台状态
  * 批次与曝光机台的匹配
  * 已挂载光罩不可移位到其他曝光机
  * 未挂载光罩仅可作用于一台曝光机
* 时间策略
  * 挂载 Mask：30 分钟
  * 增加 Recipe：45 分钟
  * 处置 Hold：120 分钟
* 期望结果
  * 有效利用所有曝光机

## 设计 Design
光罩计划使用 OptaPlanner 作为求解的工具，整个过程经过两次的模型设计。

### 模型一
自动产生批次的执行次序（Chained Planning）。此设计过于复杂，且无法求出正确解。

### 模型二
仅确认批次与曝光机台的批配是否适当。在全体组合无误后（Hard），根据批次分布状况与机台嫁动时间判定优劣（Soft）。

1. 利用 OptaPlanner 对批次与曝光机台进行组合并求得 Hard Score，Hard=0 表示没有组合违反「限制条件」。
2. 利用 SAssignmentSorter 搭配「时间策略」进行作业执行序列规划。
3. 利用 SAssignmentSorter 的结果计算出 Soft Score，Soft 分数越高表示「期望结果」越好。

类别图如下：

```mermaid
classDiagram
DispatchSolution --* SAssignment : @PlanningEntityCollectionProperty
SAssignment ..> Sfc 
SAssignment ..> Equip  : @valueRangeProviderRefs={"equipRange"}
DispatchSolution --* Equip : @ValueRangeProvider(id="equipRange")
ScoreCalculator ..> DispatchSolution 
ScoreCalculator ..> SAssignmentSorter

<<PlanningEntity>> SAssignment
<<PlanningSolution>> DispatchSolution
<<ProblemFact>> Equip

DispatchSolution: HardSoftLongScore getScore()

SAssignment: int processStart
SAssignment: int processEnd
SAssignment: int getAvailableTime()
SAssignment: int getMaskFlag()
SAssignment: int getRecipeFlag()


Equip: String id
Equip: List~String~ capability
Equip: List~String~ masks
Equip: List~String~ recipes
Equip: boolean acceptable(Sfc sfc)

Sfc: String id
Sfc: String recipeName
Sfc: String maskId
Sfc: List~String~ capability
Sfc: int readyTime
```

OptaPlanner 利用 Hard & Soft 两种分数对「组合解」进行有效与是否为最佳的判定。在光罩计划中，Hard 会根据批次是否可以在光照机台上作业决定，只有在 Hard=0，也就是所有「组合解」都是有效后，才会进行 Soft 计算。

Soft 机算在规划过程中，历经三次设计调整：

1. 利用最后被执行批次的时间
   
    理论上此时间越早发生，表示批次嫁动密度越高。但因存在「指定机台」的逻辑，在特定机台被指派过多批次后，造成其他机台的分配不平均。

2. 增加平均权重
   
    假定批次可以被平均分配，根据分配上下限值 (0.7mean ~ 1.4mean)，根据低于或高于管制限值的批次数量，搭配「最后被执行批次的时间」重新调整 Soft 分数。
  
    这样的权重补偿，依然会被 1 的情境影响，造成其余机台执行批次是否平均，都不影响整体价动率，无法找出最佳「组合解」。

3. 采用空闲时间搭配计划时效性
   
    针对计划有效时间内的批次（八小时），计算全部机台的空闲累积时间，并搭配平均权重调整 Soft 分数。

