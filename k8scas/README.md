Cassandra 4 on K8S
===


## 定义
1. Headless Service - 提供 Pod 之间的通讯。
2. Volumes - 资料卷
3. StatefulSets - 用于建立 Pod。
4. NodePort Service - 提供外部访问。

## 规划
1. 以节点为单位，进行 Pod 的管理，并在相同的服务器上产生对应的资料卷。
2. 当 Pod 被删除后，需在同一个服务器上重新建立新的 Pod。
3. 扩展的 Pod 亦在同一服务器上。
4. 跨服务器的 Pod 亦能叢集在一起。