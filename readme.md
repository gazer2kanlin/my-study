My Study
===

## Open Source

* [OptaPlanner](opta_study.md)

* [Histogram and Normal Distribution](hgnd/README.md)

* [Vector](vector/README.md)

* [Bézier Curve](bzc/README.md)

* [Douglas–Peucker Algorithm](dp/README.md)


## Projects

* [Mask Plan Design](mask_plan_design.md)

## Ohters

* [Word Styles](wordstyle/README.md)
