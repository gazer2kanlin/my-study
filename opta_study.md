OptaPlanner Study
===

## 名词

* ProblemSolution - 方案核心，提供解决问题所需的信息。

* ProblemFact - 不随问题解决过程变化的因子。

* PlanningEntity - 随问题解决过程不停变化的单体。


## 变数 Genuine Planning Variable

```mermaid
classDiagram
MySolution --* MyEntity
MyEntity ..> MyValue : valueRangeProviderRefs={"answers"}
MySolution --* MyValue : @ValueRangeProvider(id="answers")

<<PlanningEntity>> MyEntity
<<PlanningSolution>> MySolution
<<ProblemFact>> MyValue

MySolution: @PlanningEntityCollectionProperty List~MyEntity~ entityList
MySolution: @ProblemFactCollectionProperty List~MyValue~ valueList
MyValue: String text
MyEntity : @PlanningVariable MyValue answer
```

1. MySolution - 方案核心，提供一组 MyValue 答案供 MyEntity 选择。

2. MyEntity - 待解单体。

3. MyEntity.answer - 解答，为 MySolution.valueList 其中之一。 


## 变数 Shadow Variable
当设定 Genuine Planning Variable 后，动态调整该变量内的参数，此参数即为 Shadow Variable。

根据使用情境又分 Bi-Direction 和 Chained 两种模式。

### Bi-Direction Variable 双向多对一参考变数

```mermaid
classDiagram
SourceEntity o-- TargetEntity : sourceVariableName="value1"

<<PlanningEntity>> SourceEntity
<<PlanningEntity>> TargetEntity

SourceEntity : @PlanningVariable TargetEntity value1
TargetEntity : @InverseRelationShadowVariable List~SourceEntity~ value2
```

1. SourceEntity 的 `value1` 是 Genuine Planning Variable，型别为 `TargetEntity`。

2. TargetEntity 的 `value2` 是 Shadow Variable，型别为 `List<SourceEntity>`，利用 `@InverseRelationShadowVariable(sourceVariableName="value1")` 标示追踪 SourceEntity 的 `value1` 的变化。

3. 当设定 SourceEntity.`value1` 后，TargetEntity 被通知并将此 SourceEntity 实例加入到 `value2` 集合内。

### Bi-Direction Variable 双向一对一参考变数，又称 Chained Planning Variable

```mermaid
classDiagram
SourceEntity <.. TargetEntity : sourceVariableName="value1"

<<PlanningEntity>> SourceEntity
<<PlanningEntity>> TargetEntity

SourceEntity : @PlanningVariable TargetEntity value1
TargetEntity : @InverseRelationShadowVariable SourceEntity value2
```


1. SourceEntity 的 `value1` 是 Genuine Planning Variable，型别为 `TargetEntity`，标示 `@PlanningVariable(graphType = PlanningVariableGraphType.CHAINED, ...)`。

2. TargetEntity 的 `value2` 是 Shadow Variable，型别为 `SourceEntity`，利用 `@InverseRelationShadowVariable(sourceVariableName="value1")` 标示追踪 SourceEntity 的 `value1` 的变化。

3.当设定 SourceEntity.`value1` 后，TargetEntity 被通知并将 `value2` 参考此 SourceEntity 实例。

在此模式下，增加一种新的变量，称为 Anchor shadow variable，Anchor 的目的是将变量串接在一起。


## 案例：求取派工最佳排列次序

### 说明

将产品进行工艺排序，找出最短时间完成。工艺过程需考虑的要素包括：
* 抵达时间 readyTime：工艺最早可开始时间。
* 工艺时间 cycleTime：工艺所需时间。
* 切机时间 prepareTime：产品切换时需要额外花费的时间。

此案例采用 Chained Planning Variable 模型。

### 领域模型

* Good 产品
* Task 排序
* Proc 工艺 (Anchor)

```mermaid
classDiagram
Task <|.. Good
Task <|.. Proc
Good ..> Task: @PlanningVariable(graphType = PlanningVariableGraphType.CHAINED)
Task ..> Good: @InverseRelationShadowVariable(sourceVariableName = "prevTask")
Proc <.. Good: @AnchorShadowVariable(sourceVariableName = "prevTask")
Good ..> EndTimeListener 

<<PlanningEntity>> Good
<<PlanningEntity>> Task
<<PlanningEntity>> Proc
<<VariableListener>> EndTimeListener

Task: Good getNextGood()
Task: setNextGood(Good nextGood)
Task: Proc getProc()

Good: @PlanningVariable Task prevTask
Good: @InverseRelationShadowVariable Good nextGood
Good: @AnchorShadowVariable Proc proc
Good: int readyTime
Good: int prepareTime
Good: int cycleTime
Good: int startTime
Good: int endTime
Good: Proc getProc()
Good: Integer getEndTime() @CustomShadowVariable(variableListenerClass = "EndTimeListener")

Proc: @PlanningVariable Task prevTask
Proc: @InverseRelationShadowVariable Good nextGood
```

### 求解流程

1. Proc `P1` 以锚（Anchor）的形式出现，负责处理所有的 Good。
   1. `P1`.prevTask = null;
   2. `P1`.nextGood = null;
    ```mermaid
    flowchart LR
    Nil -.- P1.nextGood -.- null
    ```

2. 随机挑选一个 Good `G1`，交由 `P1` 执行。
   1. `G1`.nextGood = null;
   2. `G1`.prevTask = `P1`; (planning variable)
   3. `P1`.nextGood = `G1`; (shadow on)

    ```mermaid
    flowchart LR
    Nil -.- P1.nextGood <-.-> prevTask.G1.nextGood -.- null
    ```

3. 随机挑选一个 Good `G2`，接续在 `G1` 之后。
   1. `G2`.nextGood = null;
   2. `G2`.prevTask = `G1`; (planning variable)
   3. `G1`.nextGood = `G2`; (shadow on)

    ```mermaid
    flowchart LR
    Nil -.- P1.nextGood <-.-> prevTask.G1.nextGood <-.-> prevTask.G2.nextGood -.- null
    ```

4. 以此类推将所有 Good 串接。


### EndTimeListener 实作

```java
private void update(ScoreDirector<Sorter3Solution> scoreDirector, Good good) {
    Task prevTask = good.getPrevTask();
    if (prevTask != null) {
        Good g1 = good;
        if (prevTask instanceof Proc) {
            g1.setPrepareTime(CriteriaFactory.test((Proc) prevTask, g1));
        }
        else {
            g1.setPrepareTime(CriteriaFactory.test((Good) prevTask, g1));
        }
        g1.setStartTime(Math.max(prevTask.getEndTime(), g1.getReadyTime()) + g1.getPrepareTime());
        while (g1 != null) {
            scoreDirector.beforeVariableChanged(g1, "endTime");
            g1.setEndTime(g1.getStartTime() + g1.getCycleTime());
            scoreDirector.afterVariableChanged(g1, "endTime");

            Good g2 = g1.getNextGood();
            if (g2 != null) {
                g2.setPrepareTime(CriteriaFactory.test(g1, g2));
                g2.setStartTime(Math.max(g1.getEndTime(), g2.getReadyTime()) + g2.getPrepareTime());
            }
            g1 = g2;
        }
    }
}
```
