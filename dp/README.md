Ramer–Douglas–Peucker Algorithm
===

## 说明

Ramer–Douglas–Peucker Algorithm 又称 Douglas–Peucker Algorithm，主要解决的问题，是将由多点组成的折线，在减少点数后，依然能有效描绘该折线趋势的一种演算法。

## 原理

一个连续有序的 n 点点集合 $Line = (P_{1} ... P_{n})$，根据控制参数 $e$ 进行减点求取近似线 $LineR$。

1. 拣选 $P_{1}$ 和 $P_{n}$。
2. 计算 $P \in (P_{2} ... P_{n-1})$ 至 $\overline{P_{1}P_{n}}$ 的距离，拣选距离大于控制参数 $e$ 且最远的点，注意：线的左右两侧是不同的。
3. 确认步骤 2 中至多出现的两点，确保次序后得到拣选点 $P_{x}$ 以及 $P_{y}$。
4. 产生新的子线段
   
   $Line_{1} = (P_{1} ... P_{x})$ 
   
   $Line_{2} = (P_{x} ... P_{y})$ 
   
   $Line_{3} = (P_{y} ... P_{n})$ 

5. 针对产生的子线段重复 1 - 4 步骤，直到没有拣选点 $P_{x}$ 和 $P_{y}$ 产生。
6. 所有拣选点组成的新线段 $LineR$，即 $Line$ 的减点结果。


## Java 实作

```java

public List<Pt> reduce(List<Pt> line, double e) {
    int n = line.size();
    Pt p1 = line.get(0);
    Pt pn = line.get(n - 1);
    Vec vec = p1.vecTo(pn);

    int up = -1;
    int upDist = e;
    int down = -1;
    int downDist = -e;
    for(int i = 1; i < n - 1; n ++) {
        Pt p = line.get(i);
        double dist = p.distTo(p1, vec);
        if(dist > upDist) {
            up = i;
            upDist = dist;
        }
        else if(dist < -e) {
            down = i;
            downDist = dist;
        }
    }

    if(up == -1 && down == -1) {
        ArrayList<Pt> lineR = new ArrayList<>();
        lineR.add(p1);        
        lineR.add(p2);
        return lineR;        
    }
    else if(up == -1) {
        List<Pt> line1 = reduce(subset(line, 0, down), e);
        List<Pt> line2 = reduce(subset(line, down, n - 1), e);
        line2.remove(0);
        line1.addAll(line2);
        return line1;
    }
    else if(down == -1) {
        List<Pt> line1 = reduce(subset(line, 0, up), e);
        List<Pt> line2 = reduce(subset(line, up, n - 1), e);
        line2.remove(0);
        line1.addAll(line2);
        return line1;
    }
    int x = Math.min(up, down);
    int y = Math.max(up, down);
    List<Pt> line1 = reduce(subset(line, 0, x), e);
    List<Pt> line2 = reduce(subset(line, x, y), e);
    List<Pt> line3 = reduce(subset(line, y, n - 1), e);
    line3.remove(0);
    line2.remove(0);
    line1.addAll(line2);
    line1.addAll(line3);
    return line1;
}
```


## 参考

[Ramer–Douglas–Peucker algorithm](https://en.wikipedia.org/wiki/Ramer-Douglas-Peucker_algorithm)