Office Word 样式表
===
## 说明

`本文件参考 Office 365 英文版，若你使用的是其他版本，可能会有操作上的差异。`

## 设定学习

### 1. 分清楚 Normal 和 Body Text

此两种样式的主要差异在 Normal 样式为所有样式的来源，当变动 Normal 样式，会对整份文件发生直接影响，甚至意外的结果。而 Body Text 主要是针对本文。良好的习惯是把所有的本文都设定成 Body Text 样式。

在样式清单中，无特数设定下，排在第一位的样式为 Normal，而 Body Text 则被隐藏。所以第一步是把 Body Text 找到并显示出来。

1. 开启样式设定画面

    ![style](style1.png)

2. 点击左下第三个「Manage Styles」开启设定画面，切换到 Recommends 页签。

    ![manage styles](style2.png)

3. 找到 last Body Text，点击 Show 按钮，并设定排序为 1 后（Assign Value），确认完成。

    ![body text](style3.png)


### 2. 阶层式大纲

预设的大纲样式是没有标号的，当文件需要有阶层关系的标号时，需要重新设定。

1. 开启清单设定画面，点击「Define new Multilevel List...」。

    ![list](list1.png)

2. 对每个层级设定对应的样式，如 Level 1 对应到 Heading 1，以次类推。

    ![define new multilevel list](list2.png)

3. 套用到本文的章节文字上，自动加入阶层与标号。
    
    ![heading doc](list3.png)
